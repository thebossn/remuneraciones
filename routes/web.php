<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return redirect('/login');
});
*/


//Auth::routes();

/*
Route::get("/home", function() {
    return redirect("/principal");
});
*/

/* Auth and login */
Route::get("/", "HomeController@index")->name("home");
Route::get("noaccess", "HomeController@index2")->name("noaccess");
Route::get("/login", ["as" => "login", "uses" => "Auth\LoginController@showLoginForm"] );
Route::post("/login", "Auth\LoginController@login");
Route::post("/logout", "\App\Http\Controllers\Auth\LoginController@logout")->name("logout");
Route::post("/password/email", ["as" => "password.email", "uses" => "Auth\ForgotPasswordController@sendResetLinkEmail"] );
//Route::get("/password/reset", ["as" => "password.request", "uses" => "Auth\ForgotPasswordController@showLinkRequestForm"] );
//Route::post("/password/reset", "Auth\ResetPasswordController@reset");
//Route::get("/password/reset/{token}", ["as" => "password.reset", "uses" => "Auth\ResetPasswordController@showResetForm"] );


Route::group(['middleware' => 'App\Http\Middleware\LoggedMiddleware'], function()
{
    Route::group(['middleware' => 'App\Http\Middleware\SuperUMiddleware'], function()
    {
        /* Configuración */
        Route::get("/usuario/tiposusuario", "Usuario\TiposUsuario@index");
        
        /* Perfil */
        Route::get("perfil/usuarios", ["as" => "perfil.usuarios.index", "uses" => "Usuario\UsuariosController@index"]);
        Route::get("perfil/usuarios/editar/{idusuario}", ["as" => "perfil.usuarios.editar", "uses" => "Usuario\UsuariosController@editar"]);
        Route::post("perfil/usuarios/eliminar/{idusuario}", ["as" => "perfil.usuarios.eliminar", "uses" => "Usuario\UsuariosController@eliminar"]);
        Route::post("perfil/usuarios/guardar", ["as" => "perfil.usuarios.guardar", "uses" => "Usuario\UsuariosController@guardar"]);
        
        /* Conceptos */
        Route::get("/conceptos/listado", ["as" => "concepto.listado.index", "uses" => "Concepto\ConceptoController@index"]);
        

    });

    /* Empresa */

    Route::get("/empresa/administrar", ["as" => "empresa.administrar.index", "uses" => "General\EmpresaController@index"]);
    
    Route::post("/empresa/administrar", ["as" => "empresa.administrar.save", "uses" => "General\EmpresaController@save"]);
    Route::get('/empresa/administrar/edit/{id}', ['as' => 'empresa.administrar.edit', 'uses' => "General\EmpresaController@edit"]);
    Route::get('/empresa/administrar/{id}', ['as'=> 'empresa.administrar.show', 'uses' => 'General\EmpresaController@show']);
    Route::delete('/empresa/administrar/{empresa}', ['as'=> 'empresa.administrar.delete', 'uses' => 'General\EmpresaController@destroy']);
    Route::post('/empresa/administrar/upload', 'General\EmpresaController@guardarDocumento');    

    /* Ficha Trabajadore */
    Route::get("/RRHH/ficha", ["as" => "RRHH.ficha.index", "uses" => "General\FichaController@index"]);
    Route::post("/RRHH/ficha", ["as" => "RRHH.ficha.save", "uses" => "General\FichaController@save"]);
    Route::get('/RRHH/ficha/edit/{id}', ['as' => 'RRHH.ficha.edit', 'uses' => "General\FichaController@edit"]);
    Route::delete('/RRHH/ficha/{id}', ['as'=> 'RRHH.ficha.delete', 'uses' => 'General\FichaController@destroy']);
    Route::post('/RRHH/ficha/upload', 'General\FichaController@guardarDocumento');  

});
