<?php

namespace App\Libraries;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class Funciones   {

   function __construct()  {

   }

   public static function table2select($registros, $id, $descripcion)  {
      $retorno = array();
      foreach ($registros as $registro)   {
         $retorno[$registro[$id]] = $registro[$descripcion];
      }
      return $retorno;
   }
   /*
   public static function selectCiudades()  {
      $retorno = array();
      $ciudades = \maitena\Models\Ciudad::orderBy("idcomuna")->orderBy("ciudad")->get();
      foreach ($ciudades as $ciudad)   {
          $retorno[$ciudad->idciudad] = "Región: ".$ciudad->comuna->region->region." / Comuna: " .$ciudad->comuna->comuna." / Ciudad: ".$ciudad->ciudad;
      }        
      return $retorno;        
   }

   public static function selectContratosContratistas()  {
      $retorno = array();
      $ccontratos = \maitena\Models\ContratoContratista::where("fechatermino", ">=", date("Y-m-d"))->orderBy("idcontratista")->orderBy("nombrecontrato")->get();
      foreach ($ccontratos as $contrato)  {
         $retorno[$contrato->idccontrato] = "Contratista: ".$contrato->contratista->razonsocial." / Contrato: ".$contrato->nombrecontrato;
      }
      return $retorno; 
   }

   public static function selectSecciones()  {
      $retorno = array();
      $secciones = \maitena\Models\Seccion::orderBy("iddepartamento")->orderBy("seccion")->get();
      foreach ($secciones as $seccion) {
         $retorno[$seccion->idseccion] = "Departamento: ".$seccion->departamento->departamento." / Sección: ".$seccion->seccion;
      }
      return $retorno;
   }

   public static function selectPersonalActivo()    {
        $retorno = array();
        $pcontratos = \maitena\Models\ContratoPersonal::where("estado", 2)->where("fechainicio", "<=", date("Y-m-d"))
                        ->where(function($query)   { $query->where("fechatermino", null)->orWhere("fechatermino", ">=", date("Y-m-d")); } )->get();
        foreach ($pcontratos as $contrato)  {
            $retorno[$contrato->personal->idpersona] = $contrato->personal->apellidopaterno . " " . $contrato->personal->apellidomaterno . ", " .
                                                        $contrato->personal->primernombre . " " . $contrato->personal->segundonombre . "  [ " .
                                                        $contrato->personal->rut . " ]";
        }
        asort($retorno);
    return $retorno;
   }

   public static function selectPersonalActivoPR()    {
        $retorno = array();
        $pcontratos = \maitena\Models\ContratoPersonal::where("estado", 0)->where("fechainicio", "<=", date("Y-m-d"))
                    ->where(function($query)   { $query->where("fechatermino", null)->orWhere("fechatermino", ">=", date("Y-m-d")); } )->get();
        foreach ($pcontratos as $contrato)  {
            $retorno[$contrato->personal->idpersona] = $contrato->personal->apellidopaterno . " " . $contrato->personal->apellidomaterno . ", " .
                                                    $contrato->personal->primernombre . " " . $contrato->personal->segundonombre . "  [ " .
                                                    $contrato->personal->rut . " ]";
    }
    asort($retorno);
    return $retorno;
    }

    public static function selectPersonalActivoPREdit()    {
    $retorno = array();
    $pcontratos = \maitena\Models\ContratoPersonal::where("estado", 1)->where("fechainicio", "<=", date("Y-m-d"))
                ->where(function($query)   { $query->where("fechatermino", null)->orWhere("fechatermino", ">=", date("Y-m-d")); } )->get();
    foreach ($pcontratos as $contrato)  {
        $retorno[$contrato->personal->idpersona] = $contrato->personal->apellidopaterno . " " . $contrato->personal->apellidomaterno . ", " .
                                                $contrato->personal->primernombre . " " . $contrato->personal->segundonombre . "  [ " .
                                                $contrato->personal->rut . " ]";
    }
    asort($retorno);
    return $retorno;
    }

    public static function selectPersonalActivoFichaPersonal()    {
        $retorno = array();
        $pcontratos = \maitena\Models\ContratoPersonal::where("estado", 2)->where("fechainicio", "<=", date("Y-m-d"))
                    ->where(function($query)   { $query->where("fechatermino", null)->orWhere("fechatermino", ">=", date("Y-m-d")); } )->get();
        foreach ($pcontratos as $contrato)  {
            $retorno[$contrato->personal->idpersona] = $contrato->personal->apellidopaterno . " " . $contrato->personal->apellidomaterno . ", " .
                                                    $contrato->personal->primernombre . " " . $contrato->personal->segundonombre . "  [ " .
                                                    $contrato->personal->rut . " ]";
        }
        asort($retorno);
        return $retorno;
        }
        */
   /* comentarios para forzar push */


}