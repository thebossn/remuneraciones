<?php

namespace App\Entities;

class Funcionalidad   {
   public $codigo;
   public $titulo;
   public $breadcrumbs;
   public $breadcrumbs_active;

   public function __construct($cod, $tit, $bc, $bc_active) {
      $this->codigo = $cod;
      $this->titulo = $tit;
      $this->breadcrumbs = $bc;
      $this->breadcrumbs_active = $bc_active;
   }
}

?>