<?php

namespace App\Models;

use Eloquent as Model;

class Nivel_usuario extends Model
{
    public $table = 'nivel_usuario';
    public $timestamps = false;

    protected $primaryKey = 'idnivel';

    public $fillable = [
        "nivel"
    ];

    protected $casts = [
        "idnivel" => "integer",
        "nivel" => "string"
    ];

    public static $rules = [
        "nivel" => "required|max:14"
    ];

}