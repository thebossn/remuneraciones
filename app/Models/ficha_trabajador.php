<?php

namespace App\Models;

use Eloquent as Model;

class Ficha_trabajador extends Model
{
    public $table = 'ficha_trabajador';
    public $timestamps = false;

    protected $primaryKey = 'idtrabajador';

    public $fillable = [
        "idregion",
        "idcomuna"
    ];

    protected $casts = [
        "idtrabajador" => "integer",
        "rut" => "string",
        "nombres" => "string",
        "apellido_paterno" => "string",
        "apellido_materno" => "string",
        "direccion" => "string",
        "idregion" => "integer",
        "idcomuna" => "string",
        "telefono" => "string",
        "sexo" => "string",
        "email" => "string",
        "estudios" => "string",
        "oficio_profesion" => "string",
        "estado_civil" => "string",
        "fecha_nacimiento" => "string",
        "nacionalidad" => "string",
        "fecha_afiliacion" => "string",
        "contacto_emergencia" => "string",
        "fono_emergencia" => "string",
        "forma_pago" => "string",
        "banco" => "string",
        "sucursal" => "string",
        "tipo_cuenta" => "string",
        "centro_costo" => "string",
        "linea_negocio" => "string",
        "orden_trabajo" => "string",
        "caja_compensacion" => "string",
        "mutual_seguridad" => "string",
        "zona_extrema" => "string"

        
    ];

    public static $rules = [
        "rut" => "required|max:11",
        "nombres" => "required|max:45",
        "apellido_paterno" => "required|max:20",
        "apellido_materno" => "required|max:20",
        "sexo" => "required|max:15",
        "estado_civil" => "required|max:15",
        "fecha_nacimiento" => "required",
        "nacionalidad" => "required|max:20"

    ];

   
    public function region()  {
        return $this->belongsTo(\App\Models\regiones::class, "idregion", "idregion");
    }

    public function comuna()
    {
        return $this->belongsTo(\App\Models\comuna::class, "idcomuna", "idcomuna");
    }

}
