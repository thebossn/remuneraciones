<?php

namespace App\Models;

use Eloquent as Model;

class Conceptos_activos extends Model
{
    public $table = 'conceptos_activos';
    public $timestamps = false;

    protected $primaryKey = 'idconactivo';

    public $fillable = [
        "idconcepto"
    ];

    protected $casts = [
        "idconcepto" => "integer",
        "nombre" => "string",
        "valor" => "integer"
    ];

    public static $rules = [
        "nombre" => "required|max:50",
        "valor" => "required"

    ];

   
    public function concepto()  {
        return $this->belongsTo(\App\Models\conceptos::class, "idconcepto", "idconcepto");
    }

}