<?php

namespace App\Models;

use Eloquent as Model;

class Comuna extends Model
{
    public $table = 'comuna';
    public $timestamps = false;

    protected $primaryKey = 'idcomuna';

    public $fillable = [
        "idregion"
    ];

    protected $casts = [
        "idregion" => "integer",
        "nombre" => "string"
    ];

    public static $rules = [
        "nombre" => "required|max:45"
    ];

   
    public function region()  {
        return $this->belongsTo(\App\Models\regiones::class, "idregion", "idregion");
    }

}
