<?php

namespace App\Models;

use Eloquent as Model;

class Codigo_actividad extends Model
{
    public $table = 'codigo_actividad';
    public $timestamps = false;

    protected $primaryKey = 'idcodigo';

    public $fillable = [
        "idcodigo"
    ];

    protected $casts = [
        "idcodigo" => "integer",
        "codigo_act" => "string",
        "nombre_actividad" => "string",
        "afecto_iva" => "string",
        "categoria_tributaria" => "string",
        "disponible_internet" => "string"
    ];

    public static $rules = [
        
    ];

   
    public function region()  {
        return $this->belongsTo(\App\Models\Empresa::class, "idcodigo", "idcodigo");
    }

}