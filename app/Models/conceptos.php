<?php

namespace App\Models;

use Eloquent as Model;

class Conceptos extends Model
{
    public $table = 'conceptos';
    public $timestamps = false;

    protected $primaryKey = 'idconcepto';

    public $fillable = [
        "nombre"
    ];

    protected $casts = [
        "idconcepto" => "integer",
        "nombre" => "string",
        "valor" => "integer",
        "tipo" => "string",
        "activo" => "integer"
        
    ];

    public static $rules = [
        "nombre" => "required|max:100",
        "valor" => "required",
        "tipo" => "required|max:30",
        "activo" => "required"

    ];

    public function conceptos_activos()
    {
        return $this->hasMany(\maitena\Models\Comuna::class, "idconcepto", "idconcepto");
    }

}