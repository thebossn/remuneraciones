<?php

namespace App\Models;

use Eloquent as Model;

class Datos_contrato extends Model
{
    public $table = 'datos_contrato';
    public $timestamps = false;

    protected $primaryKey = 'idcontrato';

    public $fillable = [
        "idtrabajador",
        "idempresa"
    ];

    protected $casts = [
        "idcontrato" => "integer",
        "idempresa" => "integer",
        "fecha_ingreso" => "string",
        "estado_contrato" => "string",
        "fecha_modificacion" => "string",
        "fecha_salida" => "string",
        "duracion_contrato" => "string",
        "sueldo_base" => "integer",
        "tipo_remuneracion" => "string",
        "sindicato" => "string",
        "gratificacion_legal" => "string",
        "afp" => "string",
        "isapre" => "string",
        "isapre_porcentaje" => "float",
        "isapre_pesos" => "integer",
        "isapre_uf" => "float",
        "cargas_declaradas" => "integer",
        "cargas_autorizadas" => "integer",
        "cargas_especiales" => "integer",
        "cargas_invalidez" => "integer",
        "asignacion_familiar" => "string",
        "adicional_salud" => "string",
        "seguro_cesantia" => "string",
        "tipo_cargo" => "string",
        "jornada" => "string",
        "jornada_calculo_sem_corr" => "string",
        "idtrabajador" => "integer"
    ];

    public static $rules = [
        "fecha_ingreso" => "required",
        "sueldo_base" => "required",
        "tipo_remuneracion" => "required|max:20",
        "sindicato" => "required|max:30",
        "gratificacion_legal" => "required|max:5",
        "afp" => "required|max:45",
        "jornada" => "required|max:45",
        "jornada_calculo_sem_corr" => "required|max:45"

    ];

   
    public function trabajador()  {
        return $this->belongsTo(\App\Models\ficha_trabajador::class, "idtrabajador", "idtrabajador");
    }

    public function empresa()
    {
        return $this->belongsTo(\App\Models\empresa::class, "idempresa", "idempresa");
    }

}