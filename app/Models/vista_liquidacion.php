<?php

namespace App\Models;

use Eloquent as Model;

class Vista_liquidacion extends Model
{
    public $table = 'vista_liquidacion';
    public $timestamps = false;

    protected $primaryKey = 'idliquidacion';

    public $fillable = [
        "idempresa",
        "idcontrato"
    ];

    protected $casts = [
        "idliquidacion" => "integer",
        "idempresa" => "integer",
        "idcontrato" => "integer",
        "fecha_liquidacion" => "string",
        "periodo" => "string"
    ];

    public static $rules = [
        "fecha_liquidacion" => "required"
    ];

   
    public function empresa()  {
        return $this->belongsTo(\App\Models\empresa::class, "idempresa", "idempresa");
    }

    public function contrato()
    {
        return $this->belongsTo(\App\Models\datos_contrato::class, "idcontrato", "idcontrato");
    }

}