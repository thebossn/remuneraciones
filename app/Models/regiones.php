<?php

namespace App\Models;

use Eloquent as Model;

class Regiones extends Model
{
    public $table = 'regiones';
    public $timestamps = false;

    protected $primaryKey = 'idregion';

    public $fillable = [
        "idregion"
    ];

    protected $casts = [
        "idregion" => "integer",
        "nombre" => "string"
    ];

    public static $rules = [
        "nombre" => "required|max:45"
    ];
   
    public function comuna()  {
        return $this->hasMany(\App\Models\comuna::class, "idregion", "idregion");
    }
}