<?php

namespace App\Models;

use Eloquent as Model;

class Detalles_liquidacion extends Model
{
    public $table = 'detalles_liquidacion';
    public $timestamps = false;

    protected $primaryKey = 'iddetalle';

    public $fillable = [
        "idliquidacion",
        "idconactivos"
    ];

    protected $casts = [
        "idliquidacion" => "integer",
        "idconactivos" => "integer",
        "nombre" => "string",
        "valor" => "integer",
        "cantidad" => "integer",
        "total" => "integer"
    ];

    public static $rules = [
        "nombre" => "required",
        "valor" => "required",
        "cantidad" => "required",
        "total" => "required"

    ];

   
    public function liquidacion()  {
        return $this->belongsTo(\App\Models\vista_liquidacion::class, "idliquidacion", "idliquidacion");
    }

    public function conceptosAct()
    {
        return $this->belongsTo(\App\Models\conceptos_activos::class, "idconactivos", "idconactivos");
    }

}