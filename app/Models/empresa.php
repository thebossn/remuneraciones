<?php

namespace App\Models;

use Eloquent as Model;

class Empresa extends Model
{
    public $table = 'empresa';
    public $timestamps = false;

    protected $primaryKey = 'idempresa';

    public $fillable = [
        "idregion",
        "idcomuna",
        "iddias"
    ];

    protected $casts = [
        "idempresa" => "integer",
        "razon_social" => "string",
        "nombre_fantacia" => "string",
        "rut" => "string",
        "direccion" => "string",
        "idregion" => "integer",
        "idcomuna" => "integer",
        "telefono" => "string",
        "iddias" => "integer",
        "rut_representante" => "string",
        "nombre_representante" => "string",
        "codigo_act_economica" => "string",
        "nombre_act_economica" => "string"
    ];

    public static $rules = [
        "razon_social" => "required|max:50",
        "nombre_fantacia" => "required|max:45",
        "rut" => "required|max:11",
        "direccion" => "required|max:100",
        "rut_representante" => "required|max:11",
        "nombre_representante" => "required|max:100",
        "codigo_act_economica" => "required|max:11",
        "nombre_act_economica" => "required|max:100"

    ];

   
    public function region()  {
        return $this->belongsTo(\App\Models\regiones::class, "idregion", "idregion");
    }

    public function comuna()
    {
        return $this->belongsTo(\App\Models\comuna::class, "idcomuna", "idcomuna");
    }

    public function dias()
    {
        return $this->belongsTo(\App\Models\dias::class, "iddias", "iddias");
    }
}
