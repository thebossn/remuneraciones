<?php

namespace App\Models;

use Eloquent as Model;

class Dias_liquidacion extends Model
{
    public $table = 'dias_liquidacion';
    public $timestamps = false;

    protected $primaryKey = 'iddias';

    public $fillable = [
        "texto"
    ];

    protected $casts = [
        "texto" => "string",
        "dias" => "integer"
        
    ];

    public static $rules = [
        "texto" => "required|max:50",
        "dias" => "required"

    ];

    public function liquidaciones()
    {
        return $this->hasMany(\maitena\Models\empresa::class, "iddias", "iddias");
    }
}