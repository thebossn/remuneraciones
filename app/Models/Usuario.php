<?php

namespace App\Models;

use Eloquent as Model;

class Usuario extends Model
{
    protected   $table      = "users";
    protected   $primaryKey = "id";
    public      $timestamps = false;
    protected   $dateFormat = 'U';


    public $fillable = [
        "name",
        "email", 
        "idnivel"
    ];
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        "email" => "string",
        "idnivel" => "integer",
        "created_at" => "timestamp"
    ];
    public static $rules = [
        "name" => "required|max:255",
        "email" => "required|unique:posts|max:255",
        "idnivel" => "required"
    ];    

    public function nivelusu()  {
        return $this->belongsTo(\App\Models\Nivel_usuario::class, "idnivel", "idnivel");
    }

    
}
