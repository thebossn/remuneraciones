<?php

namespace App\Models;

use Eloquent as Model;

class Banco extends Model
{
    public $table = 'banco';
    public $timestamps = false;

    protected $primaryKey = 'idbanco';

    public $fillable = [
        "idbanco"
    ];

    protected $casts = [
        "idbanco" => "integer",
        "nombre" => "string"
    ];

    public static $rules = [
        "nombre" => "required|max:45"
    ];
   
    public function ficha_trabajador()  {
        return $this->hasMany(\App\Models\ficha_trabajador::class, "idbanco", "idbanco");
    }
}