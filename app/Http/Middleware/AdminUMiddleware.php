<?php

namespace App\Http\Middleware;

use Closure;

class AdminUMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // Forma base para la restriccion de usuarios actualmente sin uso
    public function handle($request, Closure $next)
    {
        if ($request->user()->idnivel !== 2)
		{
			return redirect('noaccess');
        }
        
        return $next($request);
    }
}
