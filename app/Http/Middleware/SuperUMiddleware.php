<?php

namespace App\Http\Middleware;

use Closure;

class SuperUMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->idnivel !== 1)
		{
			return redirect('noaccess');
        }
        
        return $next($request);
    }
}
