<?php

namespace App\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Nivel_usuario;
use App\Models\Usuario;
use Flash;
use App\Libraries\Funciones;

class UsuariosController extends Controller
{

    public function index() {
        return view("usuario.index", array("usuarios" => Usuario::all()));
    }

    public function editar($idusuario)    {
        $nivelusuario = Funciones::table2select(Nivel_usuario::orderBy("idnivel")->get(), "idnivel", "nivel");
        if ($idusuario > 0) {
            $usuario = Usuario::find($idusuario);
        } else {
            $usuario = new Usuario();
        }
        return view("usuario.edicion", array("usuario" => $usuario, "nivelusuario" => $nivelusuario));
    }

    public function guardar(Request $request)   {
        $usuario = new Usuario();
        if (!$request->id)  {
            $usuario->password = Hash::make($request->contrasena);
            $usuario->email = $request->email;
            $usuario->name = $request->email;
            $usuario->created_at = now();

        } else {
            $usuario = Usuario::find($request->id);
            if (strlen(trim($request->contrasena)) > 0) {
                $usuario->password = Hash::make($request->contrasena);
            }
        }
        $usuario->name = $request->name;
        //$usuario->active = ($request->active ? 1 : 0);
        $usuario->idnivel = $request->idnivel;
        $usuario->save();

        Flash::success('Usuario guardado correctamente.');
        return redirect(route('perfil.usuarios.index'));
    }
    
    

    public function eliminar($id)  {
        $usuario = Usuario::find($id);
        $usuario->delete();

        Flash::success('Usuario eliminado correctamente.');
        return redirect(route('perfil.usuarios.index'));
        
    }



}

