<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Funcionalidad;

class PrincipalController extends Controller
{

    public function index() {
        $funcionalidad = new Funcionalidad("prin.dash", "Dashboard", "Remuneraciones", "Dashboard");
        return view("dashboard", array("funcionalidad" => $funcionalidad));
    }


}
