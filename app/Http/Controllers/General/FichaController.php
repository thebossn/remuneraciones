<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comuna;
use App\Models\Regiones;
use App\Models\Ficha_trabajador;
use App\Models\Banco;
use Flash;
use App\Libraries\Funciones;

class FichaController extends Controller
{
    public function index()
    {
        $trabajadores = Ficha_trabajador::all();
        return view('trabajador.index', array("trabajadores" => $trabajadores));
    }

    public function edit($id)
    {
        $idtrabajador = $id;
        $trabajador = new Ficha_trabajador();
        if($idtrabajador > 0)
            $trabajador = Ficha_trabajador::find($idtrabajador);
            
        return view("trabajador.edit", array("trabajador" => $trabajador,
                                                "region" => Funciones::table2select(Regiones::orderBy("idregion")->get(), "idregion", "nombre"),
                                                "comuna" => Funciones::table2select(Comuna::orderBy("idregion")->get(), "idcomuna", "nombre"),
                                                "banco" => Funciones::table2select(Banco::orderBy("idbanco")->get(), "idbanco", "nombre"),
                                                "idtrabajador" => $idtrabajador));
    }

    
    public function save(Request $request)   {

        $idtrabajador = $request->idtrabajador;
        $trabajador = new Ficha_trabajador();

        if ($idtrabajador > 0){
            $trabajador = Ficha_trabajador::find($idtrabajador);
        }
    
        $trabajador->rut = $request->rut;
        $trabajador->nombres = $request->nombres;
        $trabajador->apellido_paterno = $request->apellido_paterno;
        $trabajador->apellido_materno = $request->apellido_materno;
        $trabajador->direccion = $request->direccion;
        $trabajador->idregion = $request->idregion;
        $trabajador->idcomuna = $request->idcomuna;
        $trabajador->telefono = $request->telefono;
        $trabajador->sexo = $request->sexo;
        $trabajador->email = $request->email;
        $trabajador->foto = $request->foto;
        $trabajador->estudios = $request->estudios;
        $trabajador->oficio_profesion = $request->oficio_profesion;
        $trabajador->estado_civil = $request->estado_civil;
        $trabajador->fecha_nacimiento = $request->fecha_nacimiento;
        $trabajador->nacionalidad = $request->nacionalidad;
        $trabajador->fecha_afiliacion = $request->fecha_afiliacion;
        $trabajador->contacto_emergencia = $request->contacto_emergencia;
        $trabajador->fono_emergencia = $request->fono_emergencia;
        $trabajador->forma_pago = $request->forma_pago;
        $trabajador->idbanco = $request->idbanco;
        $trabajador->sucursal = $request->sucursal;
        $trabajador->tipo_cuenta = $request->tipo_cuenta;
        $trabajador->numero_cuenta = $request->numero_cuenta;
        $trabajador->centro_costo = $request->centro_costo;
        $trabajador->linea_negocio = $request->linea_negocio;
        $trabajador->orden_trabajo = $request->orden_trabajo;
        $trabajador->caja_compensacion = $request->caja_compensacion;
        $trabajador->mutual_seguridad = $request->mutual_seguridad;
        $trabajador->zona_extrema = $request->zona_extrema;


        

        $trabajador->save();

        Flash::success('Datos del trabajador guardados correctamente.');
        return redirect(route('RRHH.ficha.index'));
    }

  public function destroy($idtrabajador)
    {
        
        $trabajor = Ficha_trabajador::where('idtrabajador', $idtrabajador)->first();


        if (!($idtrabajador)){
            Flash::error('Trabajador no encontrado');
            
        }
        Ficha_trabajador::destroy($idtrabajador);
        Flash::success('Trabajador eliminado correctamente');
        return redirect('RRHH/ficha');

    }
}
