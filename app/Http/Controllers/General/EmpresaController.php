<?php

namespace App\Http\Controllers\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Empresa;
use App\Models\Dias_liquidacion;
use App\Models\Codigo_actividad;
use App\Models\Comuna;
use App\Models\Regiones;
use Flash;
use App\Libraries\Funciones;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresas = Empresa::all();
        return view('empresa.index', array("empresas" => $empresas));
    }

    public function edit($id)
    {
        $idempresa = $id;
        $empresa = new Empresa();
        if($id > 0)
            $empresa = Empresa::find($idempresa);
            
        return view("empresa.edit", array("empresa" => $empresa,
                                                                    "comuna" => Funciones::table2select(Comuna::orderBy("idregion")->get(), "idcomuna", "nombre"),
                                                                    "region" => Funciones::table2select(Regiones::orderBy("idregion")->get(), "idregion", "nombre"),
                                                                    "dias" => Funciones::table2select(Dias_liquidacion::orderBy("dias", "desc")->get(), "iddias", "texto"),
                                                                    "codigoact" => Funciones::table2select(Codigo_actividad::orderBy("idcodigo")->get(), "idcodigo", "nombre_actividad"),
                                                                    "idempresa" => $idempresa));
    }

    
    public function save(Request $request)   {

        $idempresa = $request->idempresa;
        $empresa = new Empresa();

        if ($idempresa > 0){
            $empresa = Empresa::find($idempresa);
        }
    
        $empresa->razon_social = $request->razon_social;
        $empresa->nombre_fantacia = $request->nombre_fantacia;
        $empresa->rut = $request->rut;
        $empresa->direccion = $request->direccion;
        $empresa->idregion = $request->idregion;
        $empresa->idcomuna = $request->idcomuna;
        $empresa->telefono = $request->telefono;
        $empresa->iddias = $request->iddias;
        $empresa->rut_representante = $request->rut_representante;
        $empresa->nombre_representante = $request->nombre_representante;
        $empresa->idcodigo = $request->idcodigo;
        

        $actividad = Codigo_actividad::find($request->idcodigo);

        $empresa->codigo_act_economica = $actividad->codigo_act;
        $empresa->nombre_act_economica = $actividad->nombre_actividad;





        $empresa->save();

        Flash::success('Empresa guardada correctamente.');
        return redirect(route('empresa.administrar.index'));
    }

  public function destroy($idempresa)
    {
        
        $empresa = Empresa::where('idempresa', $idempresa)->first();


        if (!($idempresa)){
            Flash::error('Empresa no encontrada');
            
        }
        PrDocumentos::destroy($idempresa);
        Flash::success('Empresa eliminada correctamente');
        return redirect('empresa/administrar');

    }
    
}
