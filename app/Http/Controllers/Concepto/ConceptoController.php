<?php

namespace App\Http\Controllers\Concepto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Conceptos;

class ConceptoController extends Controller
{
    public function index()
    {
        $conceptos = Conceptos::all();
        return view('conceptos.selecconceptos', array("conceptos" => $conceptos));
    }
}