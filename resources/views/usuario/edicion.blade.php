@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Administración de Usuarios
        </h1>
   </section>
   <div class="content col-md-6">
       
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($usuario, ['route' => ['perfil.usuarios.guardar'], 'method' => 'post']) !!}
                   {!! Form::hidden('id', null) !!}

                  <div class="form-group col-sm-8">
                     {!! Form::label('name', 'Nombre:') !!}
                     {!! Form::text('name', null, ['class' => 'form-control', "required" => true, "maxlength" => 100]) !!}
                  </div>
                  <div class="form-group col-sm-8">
                     {!! Form::label('idnivel', 'Tipo de usuario:') !!}
                     {!! Form::select('idnivel', $nivelusuario, null, ['class' => 'form-control selectpicker']) !!}
                  </div>                  
                  <div class="form-group col-sm-8">
                     {!! Form::label('email', 'Correo electrónico (usuario):') !!}
                     @if ($usuario->id > 0)
                        {!! Form::email('email', null, ['class' => 'form-control', "required" => true, "maxlength" => 150, 'readonly']) !!}
                     @else
                        {!! Form::email('email', null, ['class' => 'form-control', "required" => true, "maxlength" => 150]) !!}
                     @endif
                  </div>
                  <div class="form-group col-sm-8">
                     @if ($usuario->id > 0)
                            {!! Form::label('contrasena', 'Contraseña (sólo cambio):') !!}
                     @else
                            {!! Form::label('contrasena', 'Contraseña:') !!}
                     @endif
                     {!! Form::text('contrasena', null, ['class' => 'form-control', "required" => false, "maxlength" => 20]) !!}
                  </div>
                  <div class="form-group col-sm-12">
                     {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                     <a href="{!! route('perfil.usuarios.index') !!}" class="btn btn-default">Cancelar</a>
                  </div>
                                      


                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>

<script>
$(document).ready(function(){
   $('input[type="checkbox"]').iCheck({
   checkboxClass: 'icheckbox_minimal-blue'
   })
  });
</script>   


@endsection