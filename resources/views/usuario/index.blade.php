@extends('layouts.app')

@section('content')
    <section class="content-header col-md-9">
        <h1 class="pull-left">Administración de Usuarios</h1>
        <h1 class="pull-right">
           <a  class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" 
               href="{{env('APP_URL')}}/perfil/usuarios/editar/0">Agregar</a>
         </h1>
    </section>
    <div class="clearfix"></div>
    <div class="content col-md-9">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

               <table class="table table-bordered table-striped table-hover" id="tablaUsuarios">
                  <thead>
                        <tr>
                           <th>Tipo Usuario</th>
                           <th>Nombre</th>
                           <th>Email</th>
                           <th style="width: 100px;">Acciones</th>
                        </tr>
                  </thead>
                  <tbody>
                  @foreach($usuarios as $usuario)
                        <tr>
                           <td>{{ $usuario->nivelusu->nivel }} </td>
                           <td>{{ $usuario->name }}</td>
                           <td>{{ $usuario->email }}</td>
                           <td style="width: 100px;">
                              {!! Form::open(['route' => ['perfil.usuarios.eliminar', $usuario->id], 'method' => 'post']) !!}
                              <div class='btn-group'>
                                <a href="{!! route('perfil.usuarios.editar', [$usuario->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                @if (Auth::user()->idnivel  === 1)
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Está seguro de eliminar el usuario?')"]) !!}
                                @endif
                              </div>
                              {!! Form::close() !!}
                           </td>
                        </tr>
                  @endforeach
                  </tbody>
               </table>

            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>

    <script>
         $(document).ready( function () {
            $('#tablaUsuarios').DataTable();
            rowReorder: true;
        } );   
     </script>
         
@endsection

