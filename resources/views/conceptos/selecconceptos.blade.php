@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Reglamentos</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('/prevencion/reglamentos/edit/0') !!}">Agregar</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-bordered table-striped table-hover" id="tablaContratos">
                        <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Valor</th>
                                    <th>Tipo</th>
                                    <th>Detalle Tipo</th>
                                    <th>Activo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach($conceptos as $concepto)
                                    <tr>
                                        <td>{!! $concepto->nombre !!}</td>
                                        <td>{!! $concepto->valor !!}</td>
                                        <td>{!! $concepto->tipo !!}</td>
                                        <td>{!! $concepto->detalle_tipo !!}</td>
                                        <td>{!! $concepto->activo !!}</td>
                                       
                                            

                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>






    <script>
        $(document).ready( function () {
            crearTabla("#tablaContratos");
        }); 

  </script>    
@endsection