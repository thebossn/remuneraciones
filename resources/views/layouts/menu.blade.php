<li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> <span>Inicio</span></a></li>
@if ( Auth::user()->idnivel  == 1)
<li><a href="{{url('/perfil/usuarios')}}"><i class="glyphicon glyphicon-user"></i> <span>Administracion de Usuarios</span></a></li>
@endif

<li class="treeview">
    <a href="#">
        <i class="glyphicon glyphicon-briefcase"></i><span>Empresa</span>
        <span class="pull-right-container">
        <i class="fa fa-angle-left"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="{{url('/empresa/administrar')}}"><i class="fa fa-gear"></i><span>Administrar</span>
                </span>
            </a>
            <li>
                <a href="#"><i class="glyphicon glyphicon-screenshot"></i><span>Seleccionar</span></a>
            </li>                        

        </li>
        
    </ul>
</li>
<li class="treeview">

    <a href="#">
        <i class="fa fa-users"></i><span>Recursos Humanos</span>
        <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">  

        <li  class="">
            <a href="{{url('/RRHH/ficha')}}"><i class="fa fa-user"></i><span>Fichas de Trabajadores</span></a> 
        </li>
        <li  class="">
            <a href="#"><i class="fa fa-user"></i><span>Prestaciones</span></a> 
        </li>
        <li  class="">
            <a href="#"><i class="fa fa-user"></i><span>Capacitaciones</span></a> 
        </li>          
        <li class="treeview">
            <a href="#">
                <i class="fa fa-gear"></i><span>Reclutamiento</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li  class="">
                    <a href="#"><i class="fa  fa-check-square-o"></i><span>Pre-Contrato</span></a> 
                </li>
                <li  class="">
                    <a href="#"><i class="fa  fa-check-square-o"></i><span>Contratos Pendientes</span></a> 
                </li>                  
                
            </ul>
        </li>

  
    
    </ul>
</li>

<li class="treeview">

    <a href="#">
        <i class="fa fa-folder-open"></i><span>Administración</span>
        <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">  

        <li class="treeview">
            <a href="#">
                <i class="fa fa-industry"></i><span>Contratistas</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li  class="">
                    <a href="#"><i class="fa fa-industry"></i><span>Contratistas</span></a>
                </li>                          
                <li  class="">
                    <a href="#"><i class="fa fa-file-o"></i><span>Contratos</span></a>
                </li>                
            </ul>
        </li>
    
    </ul> 
</li>










