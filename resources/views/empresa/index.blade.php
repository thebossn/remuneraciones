@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Empresas</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('/empresa/administrar/edit/0') !!}">Agregar</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                    <div class="box-body">

                        <table class="table table-bordered table-striped table-hover" id="tablaTrabajador">
                                <thead>
                                        <tr>
                                            <th>Rut</th>
                                            <th>Razon Social</th>
                                            <th>Nombre Representante</th>
                                            <th>Telefono</th>
                                            <th colspan="width: 100px">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($empresas as $empresa)
                                            <tr>
                                                <td>{!! $empresa->rut !!}</td>
                                                <td>{!! $empresa->razon_social !!}</td>
                                                <td>{!! $empresa->nombre_representante !!}</td>
                                                <td>{!! $empresa->telefono !!}</td>                                             
                                                <td>
                                                    
                                                    <div class='btn-group'>
                                                        {!! Form::open(['route' => ['empresa.administrar.delete', $empresa->idempresa], 'method' => 'delete']) !!}
                                                        <a href="{!! route('empresa.administrar.edit', [$empresa->idempresa]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                                    
                                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                        
                                                        {!! Form::close() !!}
                                                        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
        
                    </div>
        </div>
    </div>

        <script>
         $(document).ready( function () {
            $('#tablaTrabajador').DataTable();
        } );   
     </script>
  
@endsection