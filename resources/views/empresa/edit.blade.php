@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Empresa</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

              <div class="row">
                  <div class="col-md-12">
                    
                        {{ Form::model($empresa, array('action' => array('General\EmpresaController@save'), "id" => "formEmpresa", "name" => "formEmpresa", "accept-charset" => "UTF-8", "enctype" => "multipart/form-data"))   }}
                    <Form>

                    
                    {{ Form::hidden('idempresa', null, array("id" => "idempresa")) }}
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::label('rut', 'Rut', array("class" => "control-label"))   }}
                                {{ Form::text('rut', null, array("id" => "rut", "class" => "form-control", "required" => true, "maxlength" => 11)) }}
                            </div>

                            <div class="col-md-1">
                            </div>

                            <div class="col-md-4">
                                {{ Form::label('razon_social', 'Razon Social', array("class" => "control-label"))   }}
                                {{ Form::text('razon_social', null, array("id" => "razon_social", "class" => "form-control", "required" => true, "maxlength" => 50)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('nombre_fantacia', 'Nombre Fantacia', array("class" => "control-label"))   }}
                                {{ Form::text('nombre_fantacia', null, array("id" => "nombre_fantacia", "class" => "form-control", "required" => true, "maxlength" => 45)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('direccion', 'Direccion', array("class" => "control-label"))   }}
                                {{ Form::text('direccion', null, array("id" => "direccion", "class" => "form-control", "required" => true, "maxlength" => 100)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('idregion', 'Región', array("class" => "control-label"))   }}
                                {{ Form::select('idregion', $region, null , array("id" => "idregion", "class" => "form-control", "required" => true)) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::label('idcomuna', 'Comuna', array("class" => "control-label"))   }}
                                {{ Form::select('idcomuna', $comuna, null , array("id" => "idcomuna", "class" => "form-control", "required" => true)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-3">
                                {{ Form::label('telefono', 'Telefono', array("class" => "control-label"))   }}
                                {{ Form::text('telefono', null, array("id" => "telefono", "class" => "form-control", "maxlength" => 13)) }}
                            </div>

                            <div class="col-md-1">
                            </div>

                            <div class="col-md-3">
                                {{ Form::label('iddias', 'Días base Liquidación', array("class" => "control-label"))   }}
                                {{ Form::select('iddias', $dias, null , array("id" => "iddias", "class" => "form-control", "required" => true)) }}
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-3">
                                {{ Form::label('rut_representante', 'Rut Representante', array("class" => "control-label"))   }}
                                {{ Form::text('rut_representante', null, array("id" => "rut_representante", "class" => "form-control", "required" => true, "maxlength" => 11)) }}
                            </div>

                            <div class="col-md-1">
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('nombre_representante', 'Nombre Representante', array("class" => "control-label"))   }}
                                {{ Form::text('nombre_representante', null, array("id" => "nombre_representante", "class" => "form-control", "required" => true, "maxlength" => 100)) }}
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-6">
                                {{ Form::label('idcodigo', 'Código Actividad Económica', array("class" => "control-label"))   }}
                                {{ Form::select('idcodigo', $codigoact, null , array("id" => "idcodigo", "class" => "form-control", "required" => true)) }}
                            </div>
                        </div>
                        <div class="form-group col-sm-12" style="margin-top: 10px;">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('empresa.administrar.index') !!}" class="btn btn-default">Cancel</a>
                        </div>
                    
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
    @endsection