
                        
                            <div class="col-md-6">
                                    <form method="POST" id="dropzoneDoc" class="dropzone" action="{{url('/prevencion/charlas/upload')}}" accept-charset="UTF-8" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="iddocumento" value="{{ $iddocumento }}">
                                           <div class="fallback">
                                                <input type="file" name="file">                
                                           </div>
                                    </form>
                            </div>
                                    <script>
                                        $(document).ready( function () {
                                            crearTabla("#tablaprestamo");
                                        });
                                    </script>

                    <div class="col-md-7">
                        <div class="col-md-0" style="margin-top:11px">
                            <a href="{{url('/prevencion/charlas')}}" class="btn btn-primary">Terminar</a>
                        </div>
                    </div>

    <script>
            Dropzone.options.dropzoneDoc = {
                uploadMultiple: false,
                maxFiles: 1,
                acceptedFiles:".pdf"
            }
    </script>