@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1 class="pull-left">Ficha Trabajador</h1>
    <h1 class="pull-right">
    </h1>
</section>
<div class="content">
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

              <div class="row">
                  <div class="col-md-12">
                    
                        {{ Form::model($trabajador, array('action' => array('General\FichaController@save'), "id" => "formFicha", "name" => "formFicha", "accept-charset" => "UTF-8", "enctype" => "multipart/form-data"))   }}

                    {{ Form::hidden('idtrabajador', null, array("id" => "idtrabajador")) }}
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::label('rut', 'Rut', array("class" => "control-label"))   }}
                                {{ Form::text('rut', null, array("id" => "rut", "class" => "form-control", "required" => true, "maxlength" => 11)) }}
                            </div>
                            <div class="col-md-1">
                            </div>

                            <div class="col-md-4">
                                {{ Form::label('nombres', 'Nombres', array("class" => "control-label"))   }}
                                {{ Form::text('nombres', null, array("id" => "nombres", "class" => "form-control", "required" => true, "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('apellido_paterno', 'Apellido Paterno', array("class" => "control-label"))   }}
                                {{ Form::text('apellido_paterno', null, array("id" => "apellido_paterno", "class" => "form-control", "required" => true, "maxlength" => 20)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('apellido_materno', 'Apellido Materno', array("class" => "control-label"))   }}
                                {{ Form::text('apellido_materno', null, array("id" => "apellido_materno", "class" => "form-control", "required" => true, "maxlength" => 20)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('telefono', 'Teléfono', array("class" => "control-label"))   }}
                                {{ Form::text('telefono', null, array("id" => "telefono", "class" => "form-control", "maxlength" => 12)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('direccion', 'Direccion', array("class" => "control-label"))   }}
                                {{ Form::text('direccion', null, array("id" => "direccion", "class" => "form-control", "maxlength" => 100)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('idregion', 'Región', array("class" => "control-label"))   }}
                                {{ Form::select('idregion', $region, null , array("id" => "idregion", "class" => "form-control", "required" => true)) }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::label('idcomuna', 'Comuna', array("class" => "control-label"))   }}
                                {{ Form::select('idcomuna', $comuna, null , array("id" => "idcomuna", "class" => "form-control", "required" => true)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('sexo', 'Sexo', array("class" => "control-label"))   }}
                                {{ Form::select('sexo',['Hombre','Mujer','No especifica'], null , array("id" => "sexo", "class" => "form-control", "required" => true)) }}

                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('email', 'Correo Electrónico', array("class" => "control-label"))   }}
                                {{ Form::text('email', null, array("id" => "email", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('estudios', 'Estudios', array("class" => "control-label"))   }}
                                {{ Form::text('estudios', null, array("id" => "estudios", "class" => "form-control", "maxlength" => 50)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('oficio_profesion', 'Oficio/Profesión', array("class" => "control-label"))   }}
                                {{ Form::text('oficio_profesion', null, array("id" => "oficio_profesion", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('estado_civil', 'Estado Civil', array("class" => "control-label"))   }}
                                {{ Form::select('estado_civil',['Soltero/a','Casado/a','Viudo/a','Divorciado/a','Separado/a'], null , array("id" => "estado_civil", "class" => "form-control", "required" => true)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('nacionalidad', 'Nacionalidad', array("class" => "control-label"))   }}
                                {{ Form::text('nacionalidad', null, array("id" => "nacionalidad", "class" => "form-control", "maxlength" => 20)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('fecha_nacimiento', 'Fecha Nacimiento', array("class" => "control-label"))   }}
                                {{ Form::text('fecha_nacimiento', null, array("id" => "fecha_nacimiento", "class" => "form-control", "maxlength" => 15)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('fecha_afiliacion', 'Fecha Afiliación', array("class" => "control-label"))   }}
                                {{ Form::text('fecha_afiliacion', null, array("id" => "fecha_afiliacion", "class" => "form-control", "maxlength" => 20)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('contacto_emergencia', 'Nombre Contacto Emergencia', array("class" => "control-label"))   }}
                                {{ Form::text('contacto_emergencia', null, array("id" => "contacto_emergencia", "class" => "form-control", "maxlength" => 30)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('fono_emergencia', 'Fono Contacto Emergencia', array("class" => "control-label"))   }}
                                {{ Form::text('fono_emergencia', null, array("id" => "fono_emergencia", "class" => "form-control", "maxlength" => 12)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('forma_pago', 'Forma de Pago', array("class" => "control-label"))   }}
                                {{ Form::text('forma_pago', null, array("id" => "forma_pago", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('idbanco', 'Banco', array("class" => "control-label"))   }}
                                {{ Form::select('idbanco', $banco, null , array("id" => "idbanco", "class" => "form-control", "required" => true)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('sucursal', 'Sucursal', array("class" => "control-label"))   }}
                                {{ Form::text('sucursal', null, array("id" => "sucursal", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('tipo_cuenta', 'Tipo de Cuenta', array("class" => "control-label"))   }}
                                {{ Form::text('tipo_cuenta', null, array("id" => "tipo_cuenta", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('numero_cuenta', 'Número de Cuenta', array("class" => "control-label"))   }}
                                {{ Form::text('numero_cuenta', null, array("id" => "numero_cuenta", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('centro_costo', 'Centro de Costo', array("class" => "control-label"))   }}
                                {{ Form::text('centro_costo', null, array("id" => "centro_costo", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('linea_negocio', 'Linea de Negocio', array("class" => "control-label"))   }}
                                {{ Form::text('linea_negocio', null, array("id" => "linea_negocio", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('orden_trabajo', 'Orden de Trabajo', array("class" => "control-label"))   }}
                                {{ Form::text('orden_trabajo', null, array("id" => "orden_trabajo", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-4">
                                {{ Form::label('caja_compensacion', 'Caja de Compensación', array("class" => "control-label"))   }}
                                {{ Form::text('caja_compensacion', null, array("id" => "caja_compensacion", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                            
                            <div class="col-md-4">
                                {{ Form::label('mutual_seguridad', 'Mutual de Seguridad', array("class" => "control-label"))   }}
                                {{ Form::text('mutual_seguridad', null, array("id" => "mutual_seguridad", "class" => "form-control", "maxlength" => 100)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                {{ Form::label('zona_extrema', 'Zona Extrema', array("class" => "control-label"))   }}
                                {{ Form::text('zona_extrema', null, array("id" => "zona_extrema", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                {{ Form::label('foto', 'Foto del Trabajador', array("class" => "control-label"))   }}
                                {{ Form::text('foto', null, array("id" => "foto", "class" => "form-control", "maxlength" => 45)) }}
                            </div>
                        </div>

                        <div class="form-group col-sm-12" style="margin-top: 10px;">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('RRHH.ficha.index') !!}" class="btn btn-default">Cancel</a>
                        </div>
                    
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
    @endsection