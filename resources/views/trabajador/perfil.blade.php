@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Charlas</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('/prevencion/charlas/edit/0') !!}">Agregar</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                    <div class="box-body">

                        <table class="table table-bordered table-striped table-hover" id="tablaContratos">
                                <thead>
                                        <tr>
                                            <th>Numero</th>
                                            <th>Nombre</th>
                                            <th>Sigla</th>
                                            <th>Descripcion</th>
                                            <th>Archivo</th>
                                            <th colspan="width: 100px">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           
                                            <tr>
                                                <td>hola 1</td>
                                                <td>hola 1</td>
                                                <td>hola 1</td>
                                                <td>hola 1</td>
                                                <td>hola 1</td>
                                                <td>hola 1</td>

                                            </tr>
                                            
                                    </tbody>
                                </table>
        
                    </div>
        </div>
    </div>






    <script>
        $(document).ready( function () {
            crearTabla("#tablaContratos");
        }); 

  </script>    
@endsection