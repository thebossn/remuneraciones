@extends('layouts.app')

@section('content')
    <section class="content-header col-md-9">
        <h1 class="pull-left">Fichas Trabajadores</h1>
        <h1 class="pull-right">
           <a  class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" 
               href="{{env('APP_URL')}}/RRHH/ficha/edit/0">Agregar</a>
         </h1>
    </section>
    <div class="clearfix"></div>
    <div class="content col-md-9">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

               <table class="table table-bordered table-striped table-hover" id="tablaTrabajador">
                  <thead>
                        <tr>
                           <th>Rut</th>
                           <th>Nombre Completo</th>
                           <th>Telefono</th>
                           <th>email</th>
                           <th>Profesión</th>
                           <th style="width: 100px;">Detalles</th>
                        </tr>
                  </thead>
                  <tbody>
                  @foreach($trabajadores as $trabajador)
                        <tr>
                           <td>{{ $trabajador->rut }} </td>
                           <td>{{ $trabajador->nombres." ". $trabajador->apellido_paterno." ".$trabajador->apellido_materno}}</td>
                           <td>{{ $trabajador->telefono }}</td>
                           <td>{{ $trabajador->email }}</td>
                           <td>{{ $trabajador->oficio_profesion }}</td>
                           <td style="width: 100px;">
                              {!! Form::open(['route' => ['RRHH.ficha.delete', $trabajador->idtrabajador], 'method' => 'post']) !!}
                              <div class='btn-group'>
                                <a href="{!! route('RRHH.ficha.edit', [$trabajador->idtrabajador]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Está seguro de eliminar el usuario?')"]) !!}

                              </div>
                              {!! Form::close() !!}
                           </td>
                        </tr>
                  @endforeach
                  </tbody>
               </table>

            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>

    <script>
         $(document).ready( function () {
            $('#tablaTrabajador').DataTable();
            
        } );   
     </script>
         
@endsection

